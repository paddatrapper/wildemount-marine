# Wildemount Marine

This is a 2D game based on the Godot Game engine. It chronicals the adventures
of a Dungeons and Dragons party as they explore the marine world of Wildemount.

## Requirements

* Godot v3
* SCons build tool
* GCC C++ compiler

```
git submodule update --init --recursive
cd godot-cpp
scons platform=<platform> generate_bindings=yes -j4
cd ..
```

## Building

```
scons platform=<platform> -j4
```

## Disclamer
Dungeons and Dragons, D&D and Wildemount are the property of their respective
copyright owners.
