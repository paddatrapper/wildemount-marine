#include "player.h"

using namespace godot;

Player::Player() {}

Player::~Player() {}

void Player::_init() {
  name = "Sylran Rainfall";
}

void Player::_process(const double p_delta) {
}

void Player::_register_methods() {
  register_method("_process", &Player::_process);
}
