#ifndef PLAYER_H
#define PLAYER_H

#include <Godot.hpp>
#include <Sprite.hpp>
#include <string>

namespace godot {
class Player : public Sprite {
  GODOT_CLASS(Player, Sprite)

  private:
    std::string name;

  public:
    Player();
    ~Player();
    void _init();
    static void _register_methods();
    void _process(const double p_delta);
};
}

#endif
